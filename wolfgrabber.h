#ifndef WOLFGRABBER_H
#define WOLFGRABBER_H

#include <QQuickItem>
#include <QSGNode>
#include <QDebug>
#include <QtQuick/private/qsgdefaultlayer_p.h>
#include <QtQuick/private/qquickitem_p.h>
#include <QtQuick/private/qquickwindow_p.h>
#include <QQuickWindow>
#include <QPointer>
#include <QImage>

#include <QOpenGLFunctions>
#include <gl/gl.h>
#include <gl/glu.h>

class wolfgrabber : public QQuickItem, protected QOpenGLFunctions {
    Q_OBJECT

public:
    wolfgrabber(QQuickItem *parent = 0) : QQuickItem(parent){ busy = ready = false; }
    ~wolfgrabber(){}



    Q_INVOKABLE bool grab(QQuickItem *item, QString file){
        if(busy) {
            emit failed("wolfGrabber is still busy");
            return false;
        }
        if(item == 0 || !item->window() || !item->window()->isVisible()){
            emit failed("Target item/window is null or the window is not visible");
            return false;
        }

        ready  = false;
        target = item;
        window = item->window();
        saveLocation = file;

        QQuickItemPrivate::get(target)->refFromEffectItem(false);
//            QQuickWindowPrivate::get(item->window())->updateDirtyNode(item);
//            Q_ASSERT(QQuickItemPrivate::get(item)->rootNode());

        target->window()->update();

        connect(window.data(),SIGNAL(beforeSynchronizing()),
                    this,SLOT(readyUp()),Qt::DirectConnection);

        connect(window.data(),SIGNAL(afterRendering()),
                    this,SLOT(capture()),Qt::DirectConnection);

        return true;
    }

signals:
    void saved(QString path);
    void failed(QString message);

private:
    QPointer<QQuickWindow> window;
    QPointer<QQuickItem>   target;
    bool                   ready ;
    bool                   busy  ;
    QString                saveLocation;

    Q_INVOKABLE void readyUp() { ready = true; }

    Q_INVOKABLE void capture() {
        if(!ready || target == 0)   //same as saying target is null
            return;


        uint w = target->boundingRect().width();
        uint h = target->boundingRect().height();
        uint numPixels = w * h * 4;
        uchar * pixels = new uchar[numPixels];
        for(uint i = 0; i < numPixels; i++ )
            pixels[i] = 0;



        QSGRenderContext *rc = QQuickWindowPrivate::get(window)->context;
        QSGLayer    *texture = rc->sceneGraphContext()->createLayer(rc);


//        qDebug() << "setting texture properties";
        texture->setLive(true);
        texture->setItem(QQuickItemPrivate::get(target)->rootNode());
        texture->setRect(target->boundingRect());
        texture->setSize(target->boundingRect().size().toSize());

//            texture->setRecursive(true);
//            #ifndef QT_OPENGL_ES
//            if (QOpenGLContext::openGLModuleType() == QOpenGLContext::LibGL)
//                texture->setFormat(GL_RGBA8);
//            else
//                texture->setFormat(GL_RGBA);
//            #else
//                texture->setFormat(GL_RGBA);
//            #endif
//            texture->setHasMipmaps(false);
//        qDebug() << "scheduling update on texture";
        texture->scheduleUpdate();
        texture->updateTexture();


        qDebug() << "CALLING GL FUNCTIONS bind texture: " << texture->textureId();
        QOpenGLFunctions glFuncs(window->openglContext());
        glFuncs.glBindTexture(GL_TEXTURE_2D, texture->textureId()); //bind
        glFuncs.glReadPixels(0,0,w,h,GL_RGBA, GL_UNSIGNED_BYTE, pixels);
        glFuncs.glBindTexture(GL_TEXTURE_2D, 0);        //unbind
        QImage image(pixels,w,h,QImage::Format_ARGB32);
        image = image.rgbSwapped();
        qDebug() << "ENDED GL FUNCS" << image.byteCount() << " " << image.isNull();

////        qDebug() << "converting to image";
//        QImage image = texture->toImage();
//        qDebug() << "is image null : " << image.isNull()  << " " << image.byteCount()/1024 << " KB";
        if(image.save(saveLocation)) {
            emit saved(saveLocation);
        }
        else if(image.isNull()) {
            emit failed("Image could not be generated");
        }
        else {
            emit failed("Image could not be saved to : " + saveLocation);
        }

        delete texture;
        texture = 0;

//        qDebug() << "grab Finished" ;

        disconnect(window.data(), SIGNAL(afterRendering())     , this, SLOT(capture()));
        disconnect(window.data(), SIGNAL(beforeSynchronizing()), this, SLOT(readyUp()));
    }



    Q_INVOKABLE void capture2() {
        if(!ready || target == 0)   //same as saying target is null
            return;

        QSGRenderContext *rc = QQuickWindowPrivate::get(window)->context;
        QSGLayer    *texture = rc->sceneGraphContext()->createLayer(rc);

//        qDebug() << "setting texture properties";
        texture->setLive(true);
        texture->setItem(QQuickItemPrivate::get(target)->rootNode());
        texture->setRect(target->boundingRect());
        texture->setSize(target->boundingRect().size().toSize());

//            texture->setRecursive(true);
//            #ifndef QT_OPENGL_ES
//            if (QOpenGLContext::openGLModuleType() == QOpenGLContext::LibGL)
//                texture->setFormat(GL_RGBA8);
//            else
//                texture->setFormat(GL_RGBA);
//            #else
//                texture->setFormat(GL_RGBA);
//            #endif
//            texture->setHasMipmaps(false);
//        qDebug() << "scheduling update on texture";
        texture->scheduleUpdate();
        texture->updateTexture();

//        qDebug() << "converting to image";
        QImage image = texture->toImage();
//        qDebug() << "is image null : " << image.isNull()  << " " << image.byteCount()/1024 << " KB";
        if(image.save(saveLocation)) {
            emit saved(saveLocation);
        }
        else if(image.isNull()) {
            emit failed("Image could not be generated");
        }
        else {
            emit failed("Image could not be saved to : " + saveLocation);
        }

        delete texture;
        texture = 0;

//        qDebug() << "grab Finished" ;

        disconnect(window.data(), SIGNAL(afterRendering())     , this, SLOT(capture()));
        disconnect(window.data(), SIGNAL(beforeSynchronizing()), this, SLOT(readyUp()));
    }
};

#endif // WOLFGRABBER_H
