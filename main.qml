import QtQuick 2.5
import QtQuick.Controls 1.4
import HowlingWolf 1.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")


    WolfGrabber {
        id: wg
        anchors.fill: parent
        onFailed : console.error("Failed:", message);
        onSaved  : console.log("Success:", path);
    }

    Rectangle {
        id : grabRect
        anchors.centerIn: parent
        color : randColor()
        width : parent.width/4
        height : parent.height/4
        border.width: 1
        MouseArea {
            anchors.fill: parent
            onClicked   :  {
                wg.grab(grabRect, "C:/Users/Wolfy/Pictures/woopdeedoo.png");
                grabRect.color    = randColor()
                movableRect.color = randColor()
            }
        }

        Rectangle {
            id : movableRect
            width  : parent.width/2
            height : parent.height/2
            x : -width
            y : -height/2
            color : randColor()
            MouseArea {
                anchors.fill: parent
                drag.target: parent
            }
        }
    }





    function randColor(){
        return Qt.rgba(Math.random(),Math.random(), Math.random())
    }


}
