TEMPLATE   = app
HEADERS   += wolfgrabber.h
DISTFILES += main.qml
QT        += qml quick quick-private gui-private qml-private core-private core opengl
CONFIG    += c++11
SOURCES   += main.cpp
#LIBS += -lglut
#RESOURCES += qml.qrc
# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
