#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "wolfgrabber.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    //register our class and we're good to go
    qmlRegisterType<wolfgrabber>("HowlingWolf",1,0,"WolfGrabber");

    QQmlApplicationEngine engine;
    engine.load("main.qml");

    return app.exec();
}
